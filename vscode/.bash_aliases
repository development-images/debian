## Aliases for bash

### Use sudo for apt
alias apt='sudo apt'
alias apt-get='sudo apt-get'

### vi/vim -> neovim
alias vi='nvim'
alias vim='nvim'
